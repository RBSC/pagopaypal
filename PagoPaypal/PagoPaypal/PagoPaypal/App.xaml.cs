using PagoPaypal.Pages;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace PagoPaypal
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            ///MainPage = new MainPage();
            // The root page of your application
            MainPage = new NavigationPage(new PaymentPage());
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
